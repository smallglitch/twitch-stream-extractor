use crate::util::{HTTPClient, RequestError};
use async_trait::async_trait;

/// Surf HTTP client
pub struct SurfClient;

#[async_trait]
impl HTTPClient for SurfClient {
    async fn get(&self, url: String) -> Result<String, RequestError> {
        surf::get(url)
            .await?
            .body_string()
            .await
            .map_err(|e| e.into())
    }
}
