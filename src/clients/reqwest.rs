use crate::util::{HTTPClient, RequestError};
use async_trait::async_trait;

/// Reqwest HTTP client
pub struct ReqwestClient;

#[async_trait]
impl HTTPClient for ReqwestClient {
    async fn get(&self, url: String) -> Result<String, RequestError> {
        reqwest::get(&url).await?.text().await.map_err(|e| e.into())
    }
}
