#![deny(missing_docs)]

//!
//! Extract URLs for live streams or VODs from Twitch
//!

use hls_m3u8::MasterPlaylist;
use std::future::Future;
use util::{RequestError, StreamType};

const CLIENT_ID: &str = "kimne78kx3ncx6brgo4mv6wki5h1ko";

async fn get_playlist(
    client: impl util::HTTPClient,
    id: String,
    stream_type: StreamType,
) -> Result<MasterPlaylist, RequestError> {
    let access_token = util::get_access_token(&client, &id, &stream_type).await?;
    let playlist = client
        .get(stream_type.playlist_url(&id, access_token)?)
        .await?;

    playlist.parse::<MasterPlaylist>().map_err(|e| e.into())
}

/// Get the HLS M3U8 master playlist of the live stream with URLs of media playlists of the different available qualities
pub fn get_stream(
    client: impl util::HTTPClient,
    channel_name: String,
) -> impl Future<Output = Result<MasterPlaylist, RequestError>> {
    get_playlist(client, channel_name, StreamType::Live)
}

/// Get the HLS M3U8 master playlist of the VOD with URLs of media playlists of the different available qualities
pub fn get_vod(
    client: impl util::HTTPClient,
    channel_name: String,
) -> impl Future<Output = Result<MasterPlaylist, RequestError>> {
    get_playlist(client, channel_name, StreamType::VOD)
}

mod clients;

/// Contains all important traits and helper functions
pub mod util;
pub use hls_m3u8;
