use async_trait::async_trait;
use serde_json::Value;
use std::error::Error;
use std::fmt;

/// General error type
/// The actual error is represented as a string
#[derive(Debug)]
pub struct GeneralError(pub(crate) String);

impl fmt::Display for GeneralError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "An error occurred: {}", self.0)
    }
}

impl Error for GeneralError {}

/// The error type of all request related functions
/// (basically the whole crate)
pub type RequestError = Box<dyn Error + Send + Sync + 'static>;

/// Trait of the HTTP client
/// This can be used to use other clients than reqwest or surf
///
/// Requires the `async_trait` crate (not "required" but recommended due to comfort)
#[async_trait]
pub trait HTTPClient {
    /// Execute a GET request of the given URL
    async fn get(&self, url: String) -> Result<String, RequestError>;
}

/// Enum type to determine whether the requested playlist is of an actual live stream or a VOD
pub enum StreamType {
    /// Live stream
    Live,
    /// VOD
    VOD,
}

impl StreamType {
    /// Construct the URL to get the access token depending on the type of stream
    pub fn access_token_url(&self, id: &String) -> String {
        match self {
            StreamType::Live => format!(
                "https://api.twitch.tv/api/channels/{}/access_token?client_id={}",
                id,
                crate::CLIENT_ID
            ),
            StreamType::VOD => format!(
                "https://api.twitch.tv/api/vods/{}/access_token?client_id={}",
                id,
                crate::CLIENT_ID
            ),
        }
    }

    /// Construct the URL to get the master playlist depending on the type of stream
    pub fn playlist_url(&self, id: &String, access_token: Value) -> Result<String, RequestError> {
        let token = access_token["token"]
            .as_str()
            .ok_or(GeneralError("No token in response".into()))?;
        let sig = access_token["sig"]
            .as_str()
            .ok_or(GeneralError("No signature in response".into()))?;

        Ok(match self {
            StreamType::Live => format!(
                "https://usher.ttvnw.net/api/channel/hls/{}.m3u8?client_id={}&token={}&sig={}&allow_source&allow_audio_only", 
                id,
                crate::CLIENT_ID,
                token,
                sig,
            ),
            StreamType::VOD => format!(
                "https://usher.ttvnw.net/vod/{}.m3u8?client_id={}&token={}&sig={}&allow_source&allow_audio_only", 
                id,
                crate::CLIENT_ID,
                token,
                sig,
            ),
        })
    }
}

pub(crate) async fn get_access_token(
    client: &impl HTTPClient,
    id: &String,
    stream_type: &StreamType,
) -> Result<Value, RequestError> {
    let response = client.get(stream_type.access_token_url(id)).await?;

    serde_json::from_str::<'_, Value>(&response).map_err(|e| e.into())
}

pub use crate::clients::*;
