use twitch_stream_extractor::hls_m3u8::tags::VariantStream;
use twitch_stream_extractor::util::reqwest::ReqwestClient;

#[tokio::main]
async fn main() {
    let vod_playlist = twitch_stream_extractor::get_vod(ReqwestClient, "629995292".into())
        .await
        .unwrap();

    for stream in vod_playlist.video_streams() {
        if let VariantStream::ExtXStreamInf { uri, .. } = stream {
            println!("Quality: {}", stream.video().unwrap());
            println!("URL: {}", uri);
            println!("=====================================");
        }
    }
}
